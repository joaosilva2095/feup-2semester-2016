/* Generated By:JavaCC: Do not edit this line. Calculator2TreeConstants.java Version 5.0 */
public interface Calculator2TreeConstants
{
  public int JJTEXPRESSION = 0;
  public int JJTVOID = 1;
  public int JJTADD = 2;
  public int JJTSUB = 3;
  public int JJTMUL = 4;
  public int JJTDIV = 5;
  public int JJTTERM = 6;


  public String[] jjtNodeName = {
    "Expression",
    "void",
    "Add",
    "Sub",
    "Mul",
    "Div",
    "Term",
  };
}
/* JavaCC - OriginalChecksum=8684614095780207a0be0cd00c220aa0 (do not edit this line) */
