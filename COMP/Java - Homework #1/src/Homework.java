import java.util.Scanner;

/**
 * A class to validate a string using the following regular expression
 * a*bb* | aa*bc* | ef
 */
public class Homework {

    /**
     * Main method.
     * Will ask the user to write the string to be validated
     * @param args arguments sent to the console
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please write your expression: ");
        String line = scanner.nextLine();
        if(isValid(line))
            System.out.println("Your expression is valid!");
        else
            System.out.println("Your expession is not valid!");
    }

    /**
     * Check if a string is valid for the a*bb* | aa*bc* | ef regular expression.
     * Characters must equal the 'a', 'b', 'c', etc, in the regular expression.
     * @param line line to be validated
     * @return true if valid, false otherwise
     */
    public static boolean isValid(String line) {
        // ef
        if(line.equals("ef"))
            return true;

        int index = 0;

        // a*bb* && aa*bc*
        switch(line.charAt(index)) {
            case 'a':
                // Repeat until the first b is found
                while(true) {
                    if(index >= line.length()) break;
                    if (line.charAt(index) != 'a') break;
                    index++;
                }
                if(index >= line.length())
                    return false;

                // Repeat until the first c is found or eof
                int bCount = 0;
                while(true) {
                    if(index >= line.length()) break;
                    if (line.charAt(index) != 'b') break;
                    index++; bCount++;
                }
                if(index >= line.length())
                    return true;

                // Repeat until the eof or false
                while(true) {
                    if(index >= line.length()) break;
                    if (line.charAt(index) != 'c') break;
                    index++;
                }
                if(index >= line.length() && bCount == 1)
                    return true;
                return false;
            case 'b':
                // Repeat until the eof or return false
                while(true) {
                    if(index >= line.length()) break;
                    if (line.charAt(index) != 'b') break;
                    index++;
                }
                if(index >= line.length())
                    return true;
                return false;
        }

        return false;
    }

    /**
     * Check if a string is valid for a*bb* | aa*bc* | ef where a, b, c, e, f are generic characters.
     * @param line line to validate
     * @return true if valid, false otherwise
     */
    public static boolean isValidGeneric(String line) {
        if(line.length() < 1)
            return false;

        int index;

        // a*bb* && aa*bc*
        index = 0;
        char a = line.charAt(index);
        while(true) { // Repeat until the first b is found
            if(index >= line.length()) break;
            if (line.charAt(index) != a) break;
            index++;
        }
        if(index >= line.length()) // bb*
                return true;

        char b = line.charAt(index);
        int bCount = 0;
        while(true) { // Repeat until the first c is found
            if(index >= line.length()) break;
            if (line.charAt(index) != b) break;
            index++; bCount++;
        }
        if(index >= line.length()) // a*bb*
            return true;

        char c = line.charAt(index);
        while(true) { // Repeat until the first d is found
            if(index >= line.length()) break;
            if (line.charAt(index) != c) break;
            index++;
        }
        if(index >= line.length() && bCount == 1) // aa*bc*
            return true;

        // ef
        if(line.length() == 2)
            if(line.charAt(0) != line.charAt(1))
                return true;

        return false;
    }
}