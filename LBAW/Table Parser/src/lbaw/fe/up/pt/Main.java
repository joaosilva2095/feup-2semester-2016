import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Number of columns? "); int numCols = Integer.parseInt(scanner.nextLine().trim());
        System.out.println("Table? ");

        // Header
        for(int i = 0; i < numCols; i++)
            System.out.print("^ " + scanner.nextLine().trim() + " ");
        System.out.println("^");

        // Body
        int us = 1;
        while(scanner.hasNextLine()) {
            System.out.print("| US" + String.format("%02d", us) + " ");
            scanner.nextLine();
            for(int i = 1; i < numCols; i++)
                System.out.print("| " + scanner.nextLine().trim() + " ");
            System.out.println("|");
            us++;
        }
    }
}