import java.io.IOException;
import java.net.*;

/**
 * Client.
 * Invoke with: java Client <mcast_addr> <mcast_port> <oper> <opnd>
 * <oper> = 'register' or 'lookup'
 * <opnd>
 *     'register'
 *     <plate_number> <owner_name>'
 *     'lookup'
 *     <plate_number>
 */
public class Client {

    /**
     * Main function of the client
     * @param args arguments of the program
     */
    public static void main(String[] args) throws IOException {
        // Check number of minimum arguments
        if(args.length < 4) {
            System.out.println("Invalid number of arguments!");
            return;
        }

        // Multicast Address
        String mcastAddr = args[0];

        // Multicast Port
        int mcastPort;
        try {
            mcastPort = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            System.out.println("Invalid multicast port " + args[1]);
            return;
        }

        // Lookup service address
        System.out.println("Waiting for service broadcast message");
        String service = lookupService(InetAddress.getByName(mcastAddr), mcastPort);

        // Parse response
        String[] serviceArgs = service.trim().split(":");
        InetAddress serviceAddress = InetAddress.getByName(serviceArgs[0]);
        int servicePort = Integer.parseInt(serviceArgs[1]);

        System.out.println("Multicast: " + mcastAddr + ":" + mcastPort + " - " + serviceAddress + ":" + servicePort);

        String[] requestArgs = new String[3];
        for(int i = 2; i < args.length; i++)
            requestArgs[i - 2] = args[i];
        createRequest(serviceAddress, servicePort, requestArgs);
    }

    /**
     * Lookup the service address and port
     * @param address address of the multicast
     * @param port port of the multicast
     * @return message with address and port sent by the server
     */
    public static String lookupService(InetAddress address, int port) throws IOException {
        MulticastSocket multicastSocket = new MulticastSocket(port);
        multicastSocket.joinGroup(address);

        // Read message from server
        byte[] buffer = new byte[512];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        multicastSocket.receive(packet);

        return new String(buffer, 0, buffer.length);
    }

    /**
     * Create the request
     * @param args args of the request
     * @return response of the server
     */
    public static String createRequest(InetAddress address, int port, String[] args) {
        // Operation
        String request;
        String operation = args[0].toUpperCase();
        String operands = "";
        if(operation.equalsIgnoreCase("register")) {
            if(args.length < 3) {
                System.out.println("Invalid number of arguments!");
                return "";
            }
            String plateNumber = args[1],
                    ownerName = args[2];

            operands += plateNumber + " " + ownerName;
            request = "REGISTER " + plateNumber + " " + ownerName;
        } else if (operation.equalsIgnoreCase("lookup")) {
            String plateNumber = args[1];
            operands += plateNumber;
            request = "LOOKUP " + plateNumber;
        } else {
            System.out.println("Invalid operation " + operation);
            return "";
        }

        // Send request
        try {
            String response = sendUDPRequest(address, port, request);
            System.out.println(operation + " " + operands + " :: " + response);
            return response;
        } catch (IOException e) {
            System.out.println("Fatal error while sending request! " + e.getMessage());
            return "";
        }
    }

    /**
     * Send a UDP request to a server
     * @param address address to send the request
     * @param port port to send the request
     * @param request request to be sent
     * @return response of the server
     */
    public static String sendUDPRequest(InetAddress address, int port, String request) throws IOException {
        DatagramSocket clientSocket = new DatagramSocket();

        // Send the request
        byte[] requestData = request.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(requestData, requestData.length, address, port);
        clientSocket.send(sendPacket);

        // Receive the request
        byte[] receivedData = new byte[1024];
        DatagramPacket receivePacket = new DatagramPacket(receivedData, receivedData.length);
        clientSocket.receive(receivePacket);

        // Close client socket
        clientSocket.close();

        return new String(receivedData);
    }
}