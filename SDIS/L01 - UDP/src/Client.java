import java.io.IOException;
import java.net.*;

/**
 * Client.
 * Invoke with: java Client <host_name> <port_number> <oper> <opnd>
 * <oper> = 'register' or 'lookup'
 * <opnd>
 *     'register'
 *     <plate_number> <owner_name>'
 *     'lookup'
 *     <plate_number>
 */
public class Client {

    /**
     * Main function of the client
     * @param args arguments of the program
     */
    public static void main(String[] args) {
        // Check number of minimum arguments
        if(args.length < 4) {
            System.out.println("Invalid number of arguments!");
            return;
        }

        String response = createRequest(args);
        if(response.equalsIgnoreCase(""))
            System.out.println("ERROR");
        else
            System.out.println("RECEIVED: " + response);
    }

    /**
     * Create the request
     * @param args args of the request
     * @return response of the server
     */
    public static String createRequest(String[] args) {
        // Hostname
        String hostname = args[0];

        // Port Number
        int portNumber;
        try {
            portNumber = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            System.out.println("Invalid port " + args[1]);
            return "";
        }

        // Operation
        String request;
        String operation = args[2];
        if(operation.equalsIgnoreCase("register")) {
            if(args.length < 5) {
                System.out.println("Invalid number of arguments!");
                return "";
            }
            String plateNumber = args[3],
                    ownerName = args[4];

            request = "REGISTER " + plateNumber + " " + ownerName;
        } else if (operation.equalsIgnoreCase("lookup")) {
            String plateNumber = args[3];
            request = "LOOKUP " + plateNumber;
        } else {
            System.out.println("Invalid operation " + operation);
            return "";
        }

        // Send request
        try {
            return sendUDPRequest(hostname, portNumber, request);
        } catch (IOException e) {
            System.out.println("Fatal error while sending request! " + e.getMessage());
            return "";
        }
    }

    /**
     * Send a UDP request to a server
     * @param hostname hostname to send the request
     * @param port port to send the request
     * @param request request to be sent
     * @return response of the server
     */
    public static String sendUDPRequest(String hostname, int port, String request) throws IOException {
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress hostAddress = InetAddress.getByName(hostname);

        // Send the request
        byte[] requestData = request.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(requestData, requestData.length, hostAddress, port);
        clientSocket.send(sendPacket);
        System.out.println("SENT: " + request);

        // Receive the request
        byte[] receivedData = new byte[1024];
        DatagramPacket receivePacket = new DatagramPacket(receivedData, receivedData.length);
        clientSocket.receive(receivePacket);

        // Close client socket
        clientSocket.close();

        return new String(receivedData);
    }
}