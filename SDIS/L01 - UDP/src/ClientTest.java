import org.junit.Assert;

/**
 * Unit tests for Client.
 * The server must be up and running at localhost:1234.
 */
public class ClientTest {

    /**
     * Test the client
     * @throws Exception if an error occurs
     */
    @org.junit.Test
    public void testClient() throws Exception {
        // Test Register
        String[] registerData = new String[]{ "localhost", "1234", "register", "12-AB-34", "Joao Silva" };
        Assert.assertEquals("1", Client.createRequest(registerData).trim());
        Assert.assertEquals("-1", Client.createRequest(registerData).trim());

        registerData = new String[]{ "localhost", "1234", "register", "45-67-GF", "Manuel Maria" };
        Assert.assertEquals("2", Client.createRequest(registerData).trim());
        Assert.assertEquals("-1", Client.createRequest(registerData).trim());

        registerData = new String[]{ "localhost", "1234", "register", "98-72-TB", "Joao Silva" };
        Assert.assertEquals("3", Client.createRequest(registerData).trim());
        Assert.assertEquals("-1", Client.createRequest(registerData).trim());

        // Test Lookup
        String[] lookupData = new String[]{ "localhost", "1234", "lookup", "12-AB-34" };
        Assert.assertEquals("12-AB-34 Joao Silva", Client.createRequest(lookupData).trim());

        lookupData = new String[]{ "localhost", "1234", "lookup", "98-72-TB" };
        Assert.assertEquals("98-72-TB Joao Silva", Client.createRequest(lookupData).trim());

        lookupData = new String[]{ "localhost", "1234", "lookup", "45-67-GF" };
        Assert.assertEquals("45-67-GF Manuel Maria", Client.createRequest(lookupData).trim());

        lookupData = new String[]{ "localhost", "1234", "lookup", "AA-BB-CC" };
        Assert.assertEquals("-1", Client.createRequest(lookupData).trim());
    }
}