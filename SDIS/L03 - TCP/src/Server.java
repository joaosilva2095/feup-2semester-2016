import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Server.
 * Invoke with: java Server <port_number>
 */
public class Server {

    /**
     * Main function of the server
     * @param args arguments of the program
     */
    public static void main(String[] args) {
        // Check number of arguments
        if(args.length < 1) {
            System.out.println("Invalid number of arguments!");
            return;
        }

        // Port Number
        int portNumber;
        try {
            portNumber = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            System.out.println("Invalid port " + args[0]);
            return;
        }

        // Map with all plates
        Map<String, String> plates = new HashMap<>();

        // Create datagram server
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(portNumber);
        } catch (IOException e) {
            System.out.println("Error while creating the server! " + e.getMessage());
            return;
        }

        // Receive data
        System.out.println("Server started successfully at " + serverSocket.getInetAddress().getHostAddress() + ":" + serverSocket.getLocalPort());
        while(true) {
            // Read request
            Socket receiveSocket;
            String request;
            try {
                receiveSocket = serverSocket.accept();
                request = new BufferedReader(new InputStreamReader(receiveSocket.getInputStream())).readLine();
            } catch (IOException e) {
                System.out.println("Error while receiving data! " + e.getMessage());
                return;
            }

            // Parse request
            System.out.println("RECEIVED: " + request);

            String response;
            String requestArgs[] = request.split(" ");
            if(requestArgs.length < 2) {
                response = "-1";
            } else {
                String operation = requestArgs[0];
                // Register vehicle
                if(operation.equalsIgnoreCase("REGISTER")) {
                    if(requestArgs.length < 3) {
                        response = "-1";
                    } else {
                        String plateNumber = requestArgs[1],
                                ownerName = requestArgs[2];
                        for(int i = 3; i < requestArgs.length; i++)
                            ownerName += " " + requestArgs[i];
                        if(plates.containsKey(plateNumber)) {
                            response = "-1";
                        } else {
                            plates.put(plateNumber, ownerName);
                            response = "" + plates.size();
                        }
                    }
                } else
                // Lookup vehicle
                if(operation.equalsIgnoreCase("LOOKUP")) {
                    String plateNumber = requestArgs[1];
                    if(plates.containsKey(plateNumber)) {
                        response = plateNumber + " " + plates.get(plateNumber);
                    } else {
                        response = "-1";
                    }
                } else {
                    response = "-1";
                }
            }

            // Send response
            try {
                PrintWriter sendWriter = new PrintWriter(receiveSocket.getOutputStream());
                sendWriter.println(response);
                sendWriter.flush();
                receiveSocket.close();
            } catch (IOException e) {
                System.out.println("Error while sending response! " + e.getMessage());
                return;
            }
        }
    }
}